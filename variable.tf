variable "budget-amount" {
  description = "The budget limit amount."
  type        = string
}

variable "notification-threshold" {
  description = "The budget percentage threshold to trigger an alert."
  type        = number
}

variable "email-address" {
  description = "Email address to receive budget notifications."
  type        = list(any)

}

variable "aws_region" {
  type        = string
  description = "Defaultc region for root module"

}

variable "aws_root_name" {
  type        = string
  description = "Root name prefix to use in resource name tags"
}

variable "aws_region_name" {
  type        = string
  description = "Region name suffix to use in resource name tags"

}

variable "aws_environment_name" {
  type        = string
  description = "Environment name to use in resource name tags"

}

variable "aws_source_name" {
  type        = string
  description = "Source name of the tool that constructed the  resource to use in resource name tags"
}

variable "aws_kms_admin" {
  type        = string
  description = "Key Administrator"
}

variable "aws_kms_user" {
  type        = string
  description = "Key User"
}


variable "logging" {
  type    = list(any)
  default = []

}

variable "s3_bucket_name" {
  type = string
}

variable "target_bucket" {
  type = string

}