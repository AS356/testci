# Provides the AWS account ID to other resources
# Interpolate: data.aws_caller_identity.current.account_id
data "aws_caller_identity" "current" {}

resource "aws_s3_bucket" "tf_state" {
  bucket        = "testbackend-22"
  force_destroy = true
  lifecycle {
    prevent_destroy = false
  }

}
terraform {
  backend "s3" {
    bucket  = "testbackend-22"
    key     = "global/s3/terraform.tfstate"
    region  = "us-east-1"
    encrypt = true
  }
}


resource "aws_s3_bucket_public_access_block" "tf_state" {
  bucket                  = aws_s3_bucket.tf_state.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}


resource "aws_s3_bucket_acl" "tf_state" {
  bucket = aws_s3_bucket.tf_state.bucket
  acl    = "private"
}



resource "aws_s3_bucket_versioning" "tf_state" {
  bucket = aws_s3_bucket.tf_state.bucket
  versioning_configuration {
    status = "Enabled" //changed tfrom "Enabled" to "disabled"
  }
}



resource "aws_s3_bucket_server_side_encryption_configuration" "tf_state" {
  bucket = aws_s3_bucket.tf_state.bucket
  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.tf_state.arn
      sse_algorithm     = "aws:kms"
    }

  }

}



resource "aws_s3_bucket_logging" "tf_state" {

  for_each      = aws_budgets_budget.monthly-budget
  bucket        = aws_s3_bucket.tf_state.id
  target_bucket = var.target_bucket
  target_prefix = "log/${var.s3_bucket_name}"
}


# Provides an alias for the newly created AWS KMS key
resource "aws_kms_alias" "tf_state" {
  name          = "alias/${var.aws_root_name}-kms-terraform-${var.aws_region_name}"
  target_key_id = aws_kms_key.tf_state.id
}



##############################################################
################################
resource "aws_kms_key" "tf_state" {
  description             = "Used to encrypt and decrypt the Terraform State S3 Bucket"
  deletion_window_in_days = 10
  is_enabled              = true
  enable_key_rotation     = true


  tags = {
    Name        = "${var.aws_root_name}-kms-teraform-${var.aws_region_name}"
    Environment = var.aws_environment_name
    Source      = var.aws_source_name
  }


  policy = <<EOF
{
    "Id": "key-consolepolicy-3",
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Enable IAM User Permissions",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
            },
            "Action": "kms:*",
            "Resource": "*"
        },
        {
            "Sid": "Allow access for Key Administrators",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                  "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/${var.aws_kms_admin}"
                ]
            },
            "Action": [
                "kms:Create*",
                "kms:Describe*",
                "kms:Enable*",
                "kms:List*",
                "kms:Put*",
                "kms:Update*",
                "kms:Revoke*",
                "kms:Disable*",
                "kms:Get*",
                "kms:Delete*",
                "kms:TagResource",
                "kms:UntagResource",
                "kms:ScheduleKeyDeletion",
                "kms:CancelKeyDeletion"
            ],
            "Resource": "*"
        },
        {
            "Sid": "Allow use of the key",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                  "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/${var.aws_kms_user}"
                ]
            },
            "Action": [
                "kms:Encrypt",
                "kms:Decrypt",
                "kms:ReEncrypt*",
                "kms:GenerateDataKey*",
                "kms:DescribeKey"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}



















###########################/*

resource "aws_s3_bucket_replication_configuration" "tf_state" {
  provider = aws.us-east-1
  # Must have bucket versioning enabled first


  role   = aws_iam_role.replication.arn
  bucket = "replicationbucket20201032"


  rule {
    id = "foobar"

    filter {
      prefix = "foo"
    }

    status = "Enabled"

    delete_marker_replication {

      status = "Disabled"
    }


    destination {
      bucket        = aws_s3_bucket.tf_state.arn
      storage_class = "STANDARD"
    }

  }
}
######################################## */
resource "aws_iam_role" "replication" {
  name = "tf-iam-role-replication-12345"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}