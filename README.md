BOSS- AG BOSS DEMO


This repo contains source code featured for a GitLab Continuous Integration (CI) with Terraform


Terraform Code will configure  AWS billing to provide monthly budget of $500 USD

Gitlab CI configuration file .gitlab-ci.yml, will perform validation, plan, and apply stages.

Variables can be inserted using Gitlab group / project Variables for CI.

If desired, test the code locally by creating a terraform.tfvars file with these input settings:


budget-amount          = "600.0"
notification-threshold = 100
email-address          = ["user@example.com"]
aws_region = "us-east-1"         
aws_root_name        = "prod"
aws_region_name      = "us-east-1"
aws_environment_name = "prod"
aws_source_name      = "terraform" 
aws_kms_admin  = "AgarciaTemp" 
aws_kms_user   = "AgarciaTemp"
target_bucket  = "testbackend-22"
s3_bucket_name = "replicationbucket20201032" //see resource "aws_s3_bucket_replication_configuration" in backend

tip:
	
	Typing out terraform is long
	In PS 
	
	New-Alias -Name "tf" -Value "terraform"
	
	To replace tf and terraform
	
	Now we apply
	
	Tf apply
	
	Tf output -json
	



Takeaway:

Continuous Integration (CI) provides:
-vehicle for consistently 
-Delivering infrastructure as code in a reliable, secure, and predictable manner. 
-Learn how to take a bit of Terraform code and deliver it using GitLab CI. 
-Have fun creating awesome resources in the cloud!

Tasks:
Terraform in VSC
Gitlab Repo (CI)
Set up AWS cli and root account with approprioate with users with minimum policy
First set up 
main.tf
local state with terraform block (required provider) - since working with aws, tell terra to grab these providers
(aws block version 3.5, later upgraded to 4.2)+ region 
TELL IT TO MAKE SOMETHING- Build a resource "aws_budgets_budget" argument refrence here : https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/budgets_budget
then initalize, format, validate, plan and apply
now we created budget using main.tf with output.tf file with output commands 
$r = tf output -json | convertfrom-json
added child module to pass to parent module (connective tissue(specify toparent module where the child module is))
added notification(email) 
now connected it to git ci with/ gitconfig
we move local state to remote back end in aws cloud
tf refresh
Added to s3 using code to make bucket in terraform (resource "aws_s3_bucket")

Sidenote: did same thing with Azure, azure. created resource group, added tags, AGB0SS/tfaz (github.com)
Budget built without terraform CLI
Hand over to CI engine and build/maintain

So far
-budget code

-CI needs yml file
-instructions in root of project

Gitlab Configuration
-group
-project called testci
-yml file has to be valid
Gitlab reads this yml  
 What image when running jobs
what variables do we inject in to images (runners)
Cache information between jobs
Anchors and insertion points
Stages progressing through project
(Rules, what happensdefined in yaml) 
3 stages
Validate, plan, apply

Validate has 3 jobs
-validate only when .tf changes
-checkov is a static security checker look at code, like not exposing s3 bucket, secure from code perspective
Tflint is grabbing tf lint image and looking at how tf code and not breaking any best practices or recommendations

Basically don’t run validate if no changes

 plan stage (2)
Test plan (when a merge request)
Final plan (if main branch commit)
-production, 

Apply stage
Only trigger if main branch changes.
Like new branch or merge request
Jobs wont run on those changes

Apply previous plan in final plan

Also destroy, stage to tear all, 
Right person with right permissions
Creat and destroy

Pipeline triggers all jobs

Hand it over to ci engine,
Checks have to be performed
Consistent

Values/variables is in ci variables
Project level variables
-vault, variables not in code
-to keep it stateless
Aws group variables in there aws access key and secret access


Making changes real power of CI

Make a feature branch
-git checkout -b new-feature
Add those changes
Gita
And commit
Gitc “proper variable names and new aws version”
And push up to project
Git push -u origin new-feature
And will make new branch and you can merge…..


Before merge request we can see ci system running just validate …

Once tested, we can accept and submit
And this will trigger more pipelines
Cool

Test plan —-when merge request— cause already done

We can take a look

New-feature to main and we can approve

And delete source branch and click merge
And this will trigger final pipeline

And hand it over to apply plan…

Production push is done

We never interacted with terraform or cloud
-state file in s3
-each version can be found there

CI environment
-stop button in environment 
-will trigger destroy


-explore new ideas where secured and comfortable we can push in production


 





